#pragma once

#include "SimpleShader.h"

using namespace DirectX;

class Material
{

	SimpleVertexShader* vs;
	SimplePixelShader* ps;

public:
	Material(SimpleVertexShader * _vs, SimplePixelShader* _ps);
	~Material();

	SimpleVertexShader* getVertexShader();
	SimplePixelShader* getPixelShader();

	void PrepareShaders(XMFLOAT4X4 _worldMatrix ,XMFLOAT4X4 _viewMatrix, XMFLOAT4X4 _projectionMatrix);
};

