#include "Entity.h"


Entity::Entity(Mesh * inM, Material* _material, XMFLOAT3 inTrans, XMFLOAT3 inRot, XMFLOAT3 inScale)
{
	m = inM;
	material = _material;
	trans = inTrans;
	rot = inRot;
	scale = inScale;
	setTransMatrix();
	setRotMatrix();
	setScaleMatrix();
	setWorldMatrix();
}

Mesh * Entity::getMesh()
{
	return m;
}

void Entity::setTrans(XMFLOAT3 inTrans)
{
	trans = inTrans;
	setTransMatrix();
}

void Entity::setRot(XMFLOAT3 inRot)
{
	rot = inRot;
	setRotMatrix();
}

void Entity::setScale(XMFLOAT3 inScale)
{
	scale = inScale;
	setScaleMatrix();
}

void Entity::setTransMatrix()
{
	XMStoreFloat4x4(&transMatrix, XMMatrixTranslation(trans.x, trans.y, trans.z));
}

void Entity::setRotMatrix()
{
	XMStoreFloat4x4(&rotMatrix, XMMatrixRotationAxis(
		XMVectorSet(0, 0, 1, 0),
		rot.z
	));
	// Set it w.r.t Z Axis for now.
	
}

void Entity::setScaleMatrix()
{
	XMStoreFloat4x4(&scaleMatrix, XMMatrixScaling(scale.x, scale.y, scale.z));
}

void Entity::setWorldMatrix()
{
	// Perform the multiplication.
	XMMATRIX matTrans = XMLoadFloat4x4(&transMatrix);
	XMMATRIX matRot = XMLoadFloat4x4(&rotMatrix);
	XMMATRIX matSca = XMLoadFloat4x4(&scaleMatrix);

	XMStoreFloat4x4(&worldMatrix, XMMatrixTranspose(matSca * matRot * matTrans));
}

void Entity::PrepareShaders(XMFLOAT4X4 _viewMatrix, XMFLOAT4X4 _projectionMatrix)
{
	SimpleVertexShader* vs = material->getVertexShader();
	SimplePixelShader* ps = material->getPixelShader();

	vs->SetMatrix4x4("world", worldMatrix);
	vs->SetMatrix4x4("view", _viewMatrix);
	vs->SetMatrix4x4("projection", _projectionMatrix);

	vs->CopyAllBufferData();

	vs->SetShader();
	ps->SetShader();
}

XMFLOAT4X4 Entity::getWorldMatrix() {
	return worldMatrix;
}

Material * Entity::getMaterial()
{
	return material;
}

void Entity::moveRight(float factor)
{
	transMatrix.m[3][0] += 0.0001 * factor;
}

void Entity::moveUp(float factor)
{
	transMatrix.m[3][1] += 0.0001 * factor;
}

Entity::~Entity()
{
}
