#pragma once

// TODO: Check the includes and only include whatever is necessary..
#include "Entity.h"
#include "DXCore.h"

using namespace DirectX;

class Camera
{

	/*
	�	Camera�s position (vector3)
	�	Camera�s direction (vector3)
	�	Rotation around the X axis (float)
	�	Rotation around the Y axis (float)
	*/

	XMFLOAT4X4 ViewMatrix;
	XMFLOAT4X4 ProjectionMatrix;

	XMFLOAT3 Position;
	XMFLOAT3 Direction;
	float RotationY;
	float RotationX;

public:

	void Update();

	XMFLOAT4X4 getViewMatrix();
	XMFLOAT4X4 getProjectionMatrix();

	void MoveUp(float factor);

	void SetRotationX(float factor);
	void SetRotationY(float factor);

	void IncrementRotationY(float value);
	void IncrementRotationX(float value);

	void MoveForward(float factor);
	void MoveLeft(float factor);

	void SetProjectionMatrix(int width, int height);

	Camera(int width, int height,XMFLOAT3 _Position = XMFLOAT3(0, 0, -5), XMFLOAT3 _Direction = XMFLOAT3(0, 0, 1), float _RotationY = 0, float _RotationX = 0);
	~Camera();
};

