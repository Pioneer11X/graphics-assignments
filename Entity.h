#pragma once
#include "Mesh.h"
#include "DXCore.h"
#include "Material.h"

using namespace DirectX;

class Entity
{
	// A Mesh
	Mesh* m;

	// A Material
	Material * material;

	// A World Matrix
	XMFLOAT4X4 worldMatrix;

	// A Transformation Vector. Not a matrix, but a vector.
	XMFLOAT3 trans;
	XMFLOAT4X4 transMatrix;
	// A Rotation Vector. Not a matrix, but a vector. We are ignoring anything but about Z axis right now
	XMFLOAT3 rot;
	XMFLOAT4X4 rotMatrix;
	// A Scale Vector.
	XMFLOAT3 scale;
	XMFLOAT4X4 scaleMatrix;

public:
	Entity(Mesh *inM, Material* _material, XMFLOAT3 inTrans = XMFLOAT3(0, 0, 0), XMFLOAT3 inRot = XMFLOAT3(0, 0, 0), XMFLOAT3 inScale = XMFLOAT3(1, 1, 1) );
	Mesh* getMesh();

	// Functions to update using the vectors.
	void setTrans(XMFLOAT3 inTrans);
	void setRot(XMFLOAT3 inRot);
	void setScale(XMFLOAT3 inScale);

	// Functions if you want to tamper with the matrices themselves.
	void setTransMatrix();
	void setRotMatrix();
	void setScaleMatrix();
	void setWorldMatrix();

	void PrepareShaders(XMFLOAT4X4 _viewMatrix, XMFLOAT4X4 _projectionMatrix);

	// Getters.
	XMFLOAT4X4 getWorldMatrix();
	Material* getMaterial();

	// Animators. You need to calculate the World Matrix immediately after. Not including that in the function itself, because I do not want to call it multiple times per frame.
	void moveRight(float factor);
	void moveUp(float factor);
	~Entity();

};

