#include "Camera.h"



void Camera::Update()
{

	XMMATRIX RotationMatrix = XMMatrixRotationRollPitchYaw(RotationX, RotationY, 0);
	XMVECTOR DirectionVector = XMVector3TransformNormal(XMVectorSet(0, 0, 1, 0), RotationMatrix);

	XMStoreFloat3(&Direction, DirectionVector);

	XMStoreFloat4x4(&ViewMatrix, XMMatrixTranspose( 
		XMMatrixLookToLH(
			XMLoadFloat3(&Position),
			DirectionVector,
			XMVectorSet(0, 1, 0, 0)
		)
	));

}

XMFLOAT4X4 Camera::getViewMatrix()
{
	return ViewMatrix;
}

XMFLOAT4X4 Camera::getProjectionMatrix()
{
	return ProjectionMatrix;
}

void Camera::MoveUp(float factor)
{
	Position.y += factor * 0.001f;
}

void Camera::SetRotationX(float factor)
{
	RotationX = factor;
}

void Camera::SetRotationY(float factor)
{
	RotationY = factor;
}

void Camera::IncrementRotationY(float value)
{
	if ((RotationY + value < 0.6f) && (RotationY + value > -0.6f)) {
		RotationY += value;
	}
}

void Camera::IncrementRotationX(float value)
{
	if ((RotationX + value < 0.6f) && (RotationX + value > -0.6f))	{
		RotationX += value;
	}
}

void Camera::MoveForward(float factor)
{
	// Get the Direction you are facing, and add it to the position.
	Position.x += Direction.x * factor * 0.001f;
	Position.y += Direction.y * factor * 0.001f;
	Position.z += Direction.z * factor * 0.001f;
}

void Camera::MoveLeft(float factor)
{
	/*
	Left/right vectors can be found using the cross product of forward (camera�s direction) and the world�s up axis (0,1,0).
	*/
	XMVECTOR RightSide;
	RightSide = XMVector3Cross( XMLoadFloat3(&Direction), XMVectorSet(0, 1, 0, 0) );
	RightSide = XMVector3Normalize(RightSide);

	XMFLOAT3 RightSideFloat;
	XMStoreFloat3(&RightSideFloat, RightSide);

	Position.x += RightSideFloat.x * factor * 0.001f;
	Position.y += RightSideFloat.y * factor * 0.001f;
	Position.z += RightSideFloat.z * factor * 0.001f;

}

void Camera::SetProjectionMatrix( int width, int height)
{

	XMStoreFloat4x4(&ProjectionMatrix, XMMatrixPerspectiveFovLH(
		0.25f * 3.1415926535f,		// Field of View Angle
		(float)width / height,		// Aspect ratio
		0.1f,						// Near clip plane distance
		100.0f));					// Far clip plane distance

}

Camera::Camera(int _width, int _height, XMFLOAT3 _Position, XMFLOAT3 _Direction, float _RotationY, float _RotationX)
{
	SetProjectionMatrix(_width, _height);
	Position = _Position;
	Direction = _Direction;
	RotationX = _RotationX;
	RotationY = _RotationY;

	Update();
}

Camera::~Camera()
{
}
