#pragma once
#include "Vertex.h"
#include "DXCore.h"

class Mesh
{

	Vertex * vertices;
	int vertexCount;
	int * indices;
	int indexCount;
	ID3D11Buffer * vertexBuffer;
	ID3D11Buffer * indexBuffer;
	ID3D11Device * device;

public:
	Mesh( Vertex* inputVertices, int vertexCount, int* inputIndices, int indexCount, ID3D11Device* inputDevice );
	~Mesh();

	ID3D11Buffer* getIndexBuffer();
	ID3D11Buffer* getVertexBuffer();
	int getIndexCount();
	int getVertexCount();

};

