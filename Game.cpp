#include "Game.h"
#include "Vertex.h"
#include "Entity.h"

// For the DirectX Math library
using namespace DirectX;

// --------------------------------------------------------
// Constructor
//
// DXCore (base class) constructor will set up underlying fields.
// DirectX itself, and our window, are not ready yet!
//
// hInstance - the application's OS-level handle (unique ID)
// --------------------------------------------------------
Game::Game(HINSTANCE hInstance)
	: DXCore( 
		hInstance,		   // The application's handle
		"DirectX Game",	   // Text for the window's title bar
		1280,			   // Width of the window's client area
		720,			   // Height of the window's client area
		true)			   // Show extra stats (fps) in title bar?
{
	// Initialize fields
	vertexBuffer = 0;
	indexBuffer = 0;
	vertexShader = 0;
	pixelShader = 0;

#if defined(DEBUG) || defined(_DEBUG)
	// Do we want a console window?  Probably only in debug mode
	CreateConsoleWindow(500, 120, 32, 120);
	printf("Console window created successfully.  Feel free to printf() here.");
#endif
}

// --------------------------------------------------------
// Destructor - Clean up anything our game has created:
//  - Release all DirectX objects created here
//  - Delete any objects to prevent memory leaks
// --------------------------------------------------------
Game::~Game()
{
	// Release any (and all!) DirectX objects
	// we've made in the Game class
	if (vertexBuffer) { vertexBuffer->Release(); }
	if (indexBuffer) { indexBuffer->Release(); }

	// Delete our simple shader objects, which
	// will clean up their own internal DirectX stuff
	delete vertexShader;
	delete pixelShader;

	delete triangleMesh;
	delete squareMesh;

	delete triangleEntity;
	delete triangleEntity1;
	delete triangleEntity2;
	delete triangleEntity3;
	delete triangleEntity4;

	delete newCamera;
	delete newMaterial;
}

// --------------------------------------------------------
// Called once per program, after DirectX and the window
// are initialized but before the game loop.
// --------------------------------------------------------
void Game::Init()
{



	// Helper methods for loading shaders, creating some basic
	// geometry to draw and some simple camera matrices.
	//  - You'll be expanding and/or replacing these later
	LoadShaders();
	CreateMatrices();
	CreateBasicGeometry();

	// Tell the input assembler stage of the pipeline what kind of
	// geometric primitives (points, lines or triangles) we want to draw.  
	// Essentially: "What kind of shape should the GPU draw with our data?"
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

// --------------------------------------------------------
// Loads shaders from compiled shader object (.cso) files using
// my SimpleShader wrapper for DirectX shader manipulation.
// - SimpleShader provides helpful methods for sending
//   data to individual variables on the GPU
// --------------------------------------------------------
void Game::LoadShaders()
{
	vertexShader = new SimpleVertexShader(device, context);
	if (!vertexShader->LoadShaderFile(L"Debug/VertexShader.cso"))
		vertexShader->LoadShaderFile(L"VertexShader.cso");

	pixelShader = new SimplePixelShader(device, context);
	if (!pixelShader->LoadShaderFile(L"Debug/PixelShader.cso"))
		pixelShader->LoadShaderFile(L"PixelShader.cso");

}



// --------------------------------------------------------
// Initializes the matrices necessary to represent our geometry's 
// transformations and our 3D camera
// --------------------------------------------------------
void Game::CreateMatrices()
{
	newCamera = new Camera(width, height);
	//newCamera->SetProjectionMatrix(width, height);

}


// --------------------------------------------------------
// Creates the geometry we're going to draw - a single triangle for now
// --------------------------------------------------------
void Game::CreateBasicGeometry()
{
	// Create some temporary variables to represent colors
	// - Not necessary, just makes things more readable
	XMFLOAT4 red	= XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4 green	= XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	XMFLOAT4 blue	= XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	XMFLOAT4 gray	= XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);

	// Set up the vertices of the triangle we would like to draw
	// - We're going to copy this array, exactly as it exists in memory
	//    over to a DirectX-controlled data structure (the vertex buffer)
	Vertex triangleVertices[] = 
	{
		{ XMFLOAT3(+0.0f, +1.0f, +0.0f), red },
		{ XMFLOAT3(+1.5f, -1.0f, +0.0f), blue },
		{ XMFLOAT3(-1.5f, -1.0f, +0.0f), green },
	};

	Vertex squareVertices[] = {
		{ XMFLOAT3( 2.0f, +2.0f, 0.0f ), red },
		{ XMFLOAT3(4.0f, +2.0f, 0.0f), blue },
		{ XMFLOAT3(4.0f, +0.0f, 0.0f), green },
		{ XMFLOAT3(2.0f, +0.0f, 0.0f), gray}
	};

	// Set up the indices, which tell us which vertices to use and in which order
	// - This is somewhat redundant for just 3 vertices (it's a simple example)
	// - Indices are technically not required if the vertices are in the buffer 
	//    in the correct order and each one will be used exactly once
	// - But just to see how it's done...
	int triangleIndices[] = { 0, 1, 2 };
	int squareIndices[] = { 0, 1, 2, 0, 2, 3 };

	triangleMesh = new Mesh(triangleVertices, 3, triangleIndices, 3, device);
	squareMesh = new Mesh(squareVertices, 4, squareIndices, 6, device);
	
	newMaterial = new Material(vertexShader, pixelShader);

	triangleEntity =  new Entity(triangleMesh, newMaterial, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.3f, 0.3f, 0.3f));
	triangleEntity1 = new Entity(triangleMesh, newMaterial, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.3f, 0.3f, 0.3f));
	triangleEntity2 = new Entity(triangleMesh, newMaterial, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.3f, 0.3f, 0.3f));
	triangleEntity3 = new Entity(triangleMesh, newMaterial, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.3f, 0.3f, 0.3f));
	triangleEntity4 = new Entity(triangleMesh, newMaterial, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.3f, 0.3f, 0.3f));

}

void Game::drawEntity(Entity * e)
{

	e->PrepareShaders(newCamera->getViewMatrix(), newCamera->getProjectionMatrix());

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	//vertexShader->SetMatrix4x4("world", e->getWorldMatrix());
	//vertexShader->SetMatrix4x4("view", newCamera->getViewMatrix());
	////vertexShader->SetMatrix4x4("projection", projectionMatrix);

	//XMFLOAT4X4 projectionMatrix;
	//XMMATRIX P = XMMatrixPerspectiveFovLH(
	//	0.25f * 3.1415926535f,		// Field of View Angle
	//	(float)width / height,		// Aspect ratio
	//	0.1f,						// Near clip plane distance
	//	100.0f);					// Far clip plane distance
	//XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(P));
	//vertexShader->SetMatrix4x4("projection", projectionMatrix);

	////vertexShader->SetMatrix4x4("projection", newCamera->getProjectionMatrix());
	//vertexShader->CopyAllBufferData();

	ID3D11Buffer* vertexBuffer = e->getMesh()->getVertexBuffer();
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	context->IASetIndexBuffer(e->getMesh()->getIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

	// Finally do the actual drawing
	//  - Do this ONCE PER OBJECT you intend to draw
	//  - This will use all of the currently set DirectX "stuff" (shaders, buffers, etc)
	//  - DrawIndexed() uses the currently set INDEX BUFFER to look up corresponding
	//     vertices in the currently set VERTEX BUFFER
	context->DrawIndexed(
		e->getMesh()->getIndexCount(),     // The number of indices to use (we could draw a subset if we wanted)
		0,     // Offset to the first index we want to use
		0);    // Offset to add to each index when looking up vertices

}


// --------------------------------------------------------
// Handle resizing DirectX "stuff" to match the new window size.
// For instance, updating our projection matrix's aspect ratio.
// --------------------------------------------------------
void Game::OnResize()
{
	// Handle base-level DX resize stuff
	DXCore::OnResize();

	newCamera->SetProjectionMatrix(width, height);

	// Update our projection matrix since the window size changed
	//XMMATRIX P = XMMatrixPerspectiveFovLH(
	//	0.25f * 3.1415926535f,	// Field of View Angle
	//	(float)width / height,	// Aspect ratio
	//	0.1f,				  	// Near clip plane distance
	//	100.0f);			  	// Far clip plane distance
	//XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(P)); // Transpose for HLSL!

	//XMMATRIX P = XMLoadFloat4x4(newCamera->getProjectionMatrix());

	//XMStoreFloat4x4(&projectionMatrix, P);
}

// --------------------------------------------------------
// Update your game here - user input, move objects, AI, etc.
// --------------------------------------------------------
void Game::Update(float deltaTime, float totalTime)
{
	// Quit if the escape key is pressed
	if (GetAsyncKeyState(VK_ESCAPE))
		Quit();

	float sine = sin(totalTime);
	// This might be time consuming?
	float inc = remainderf(totalTime, float(10000));

	triangleEntity->moveRight(5);
	triangleEntity->setRot(XMFLOAT3(0, 0, inc));
	triangleEntity->setWorldMatrix();

	triangleEntity1->moveRight(-5);
	triangleEntity1->setRot(XMFLOAT3(0, 0, -inc));
	triangleEntity1->setWorldMatrix();

	triangleEntity2->moveUp(2);
	triangleEntity2->setRot(XMFLOAT3(0, 0, inc));
	triangleEntity2->setWorldMatrix();

	triangleEntity3->moveUp(-2);
	triangleEntity3->setRot(XMFLOAT3(0, 0, -inc));
	triangleEntity3->setWorldMatrix();

	if (GetAsyncKeyState('W') & 0x8000) { /* Do something useful */ 
		newCamera->MoveForward(5);
	}
	if (GetAsyncKeyState('S') & 0x8000) { /* Do something useful */ 
		newCamera->MoveForward(-5);
	}
	if (GetAsyncKeyState('A') & 0x8000) { /* Do something useful */ 
		newCamera->MoveLeft(5);
	}
	if (GetAsyncKeyState('D') & 0x8000) { /* Do something useful */ 
		newCamera->MoveLeft(-5);
	}
	if (GetAsyncKeyState('X') & 0x8000) { /* Do something useful */
		newCamera->MoveUp(-5);
	}
	if (GetAsyncKeyState(VK_SPACE) & 0x8000) { /* Do something useful */
		newCamera->MoveUp(5);
	}
	//newCamera->SetRotationX(sine);
	//newCamera->SetRotationY(sine);

	newCamera->Update();

}

// --------------------------------------------------------
// Clear the screen, redraw everything, present to the user
// --------------------------------------------------------
void Game::Draw(float deltaTime, float totalTime)
{
	// Background color (Cornflower Blue in this case) for clearing
	const float color[4] = {0.4f, 0.6f, 0.75f, 0.0f};

	// Clear the render target and depth buffer (erases what's on the screen)
	//  - Do this ONCE PER FRAME
	//  - At the beginning of Draw (before drawing *anything*)
	context->ClearRenderTargetView(backBufferRTV, color);
	context->ClearDepthStencilView(
		depthStencilView, 
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0);

	// Set buffers in the input assembler
	//  - Do this ONCE PER OBJECT you're drawing, since each object might
	//    have different geometry.


	// Using Entity's worldMatrix
	drawEntity(triangleEntity);
	drawEntity(triangleEntity1);
	drawEntity(triangleEntity2);
	drawEntity(triangleEntity3);
	drawEntity(triangleEntity4);


	// Present the back buffer to the user
	//  - Puts the final frame we're drawing into the window so the user can see it
	//  - Do this exactly ONCE PER FRAME (always at the very end of the frame)
	swapChain->Present(0, 0);
}


#pragma region Mouse Input

// --------------------------------------------------------
// Helper method for mouse clicking.  We get this information
// from the OS-level messages anyway, so these helpers have
// been created to provide basic mouse input if you want it.
// --------------------------------------------------------
void Game::OnMouseDown(WPARAM buttonState, int x, int y)
{
	// Add any custom code here...

	// Save the previous mouse position, so we have it for the future
	prevMousePos.x = x;
	prevMousePos.y = y;

	// Caputure the mouse so we keep getting mouse move
	// events even if the mouse leaves the window.  we'll be
	// releasing the capture once a mouse button is released
	SetCapture(hWnd);
}

// --------------------------------------------------------
// Helper method for mouse release
// --------------------------------------------------------
void Game::OnMouseUp(WPARAM buttonState, int x, int y)
{
	// Add any custom code here...

	// We don't care about the tracking the cursor outside
	// the window anymore (we're not dragging if the mouse is up)
	ReleaseCapture();
}

// --------------------------------------------------------
// Helper method for mouse movement.  We only get this message
// if the mouse is currently over the window, or if we're 
// currently capturing the mouse.
// --------------------------------------------------------
void Game::OnMouseMove(WPARAM buttonState, int x, int y)
{
	// Add any custom code here...

	// We find out the difference b/w the previous X and the Current X, we use to rotate about Y axis.
	if (buttonState & 0x0001) {
		long diffX = x - prevMousePos.x;
		long diffY = y - prevMousePos.y;
		newCamera->IncrementRotationY(diffX * 0.01f);
		newCamera->IncrementRotationX(diffY * 0.01f);
	}

	// Save the previous mouse position, so we have it for the future
	prevMousePos.x = x;
	prevMousePos.y = y;
}

// --------------------------------------------------------
// Helper method for mouse wheel scrolling.  
// WheelDelta may be positive or negative, depending 
// on the direction of the scroll
// --------------------------------------------------------
void Game::OnMouseWheel(float wheelDelta, int x, int y)
{
	// Add any custom code here...
}



#pragma endregion